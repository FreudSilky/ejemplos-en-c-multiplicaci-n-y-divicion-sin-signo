#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

int num,aux=1;
void factorial (){
  printf("\n\tIngresa un número entero positivo para calcular su factorial: ");
  scanf("%d", &num);
  for(int i=1; i<num+1; i++){
    aux=aux*i;
  }
  printf("\n\tEl factorial es: %d \n\n ",aux);
  system("PAUSE");
}
double decimal(double valor, const unsigned int indice)
{ /** esta funcion usa el operador de división, cambialo tomando de ejemplo la funcion siguiente **/
    unsigned int iterador = 0;
    while (iterador < indice)
    {
        valor *= pow(10,-1);
        iterador++;
    }
    return valor;
}

double division(double divisor, double dividendo, const unsigned int decimales_maximos)
{
    double cociente = 0.0;
    unsigned int decimales = 0;
    while (1)
    {
        divisor -= dividendo;
        cociente += decimal(1, decimales);
        if (divisor > 0 && divisor < dividendo && decimales < decimales_maximos)
        {
            divisor *= 10.0;
            decimales++;
        }
        else if (dividendo > divisor)
        {
            break;
        }
    }
    return cociente;
}

char delimitador[] = ".";
int punto;

void puntos(char num[])
{

    char *token = strtok(num, delimitador);
    int cont = 0;
    if (token != NULL)
    {
        while (token != NULL)
        {
            // Sólo en la primera pasamos la cadena; en las siguientes pasamos NULL
            if (cont == 0)
            {
                token = strtok(NULL, delimitador);
                cont++;
            }
            else
            {
                punto += strlen(token);
                token = strtok(NULL, delimitador);
                cont++;
            }
        }
    }
}
char *quitarPunto(char num[], char temp[])
{
    char tem[50];
    char *token = strtok(num, delimitador);
    if (token != NULL)
    {
        while (token != NULL)
        {
            // Sólo en la primera pasamos la cadena; en las siguientes pasamos NULL
            strcat(tem, token);
            token = strtok(NULL, delimitador);
        }
        strcpy(temp, tem);
        return temp;
    }
    return temp;
}

void multi()

{

    double a, b, i;
    punto = 0;
    printf("\nIngrese el primer numero: ");

    scanf("%lf", &a);

    printf("\nIngrese el segundo numero: ");

    scanf("%lf", &b);

    char num1[50];
    char num2[50];
    char temp[50];
    gcvt(a, 8, num1);
    gcvt(b, 8, num2);

    strcpy(temp, num1);
    puntos(temp);
    strcpy(temp, num2);
    puntos(temp);

    int c, d, suma = 0;
    strcpy(num1, quitarPunto(num1, temp));
    c = atoi(num1);
    strcpy(num2, quitarPunto(num2, temp));
    d = atoi(num2);

    for (i = 1; i <= c; i++)
    {

        suma += d;
    }
    char sum[50];

    itoa(suma, sum, 10);
    int largo = 1 + strlen(sum);
    int lugarPunto = (largo - punto) - 1;
    char sumaF[50] = "";
    int contPunto = 0;
    for (size_t z = 0; z < largo; z++)
    {
        if (z == lugarPunto)
        {
            sumaF[z] = '.';
        }
        else
        {
            sumaF[z] = sum[contPunto];
            contPunto++;
        }
    }
    gcvt(a, 8, num1);
    gcvt(b, 8, num2);
    strcpy(temp, sumaF);
    printf("\nEl resultado de multiplicar %s x %s es: %s\n\n", num1, num2, temp);

    system("PAUSE");
}
int main()
{
    int n, opcion;

    do
    {
        printf("\n   1. Multiplicacion.");
        printf("\n   2. Divicion.");
        printf("\n   3. Factorial.");
        printf("\n   4. Salir.");
        printf("\n\n   Introduzca opcion (1-4): ");

        scanf("%d", &opcion);

        /* Inicio del anidamiento */
        double a, b, c;
        switch (opcion)
        {
        case 1:
            multi();
            break;

        case 2:

            printf("\nIngrese el divisor: ");
            scanf("%lf", &a);
            printf("\nIngrese el dividendo: ");
            scanf("%lf", &b);
    if(a<b){
        a*=pow(10,6);
        c = division(a, b, 3);
        a*=pow(10,-6);
        c*=pow(10,-6);
            printf("\nEl resultado de la divicion %lf / %lf es: %lf\n\n", a, b, c);

            system("PAUSE");

    }else
    {
        
    
    
            c = division(a, b, 3);
            printf("\nEl resultado de la divicion %lf / %lf es: %lf\n\n", a, b, c);

            system("PAUSE");
    }

            break;

        case 3:
                factorial();
            break;
        default:
            printf("\n   Opcion no disponible \n\n");
            system("PAUSE");
            break;
        }

        /* Fin del anidamiento */

    } while (opcion != 4);

    return 0;
}